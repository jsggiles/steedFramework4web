<%@page import="steed.domain.GlobalParam"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/head/tagLib.jsp" %>
<div class="pageContent">
<div class="pageFormContent" layoutH="60">
<form target="navTab" method="post" action="<st:getCurrentUrl lastUrl="update.act" />" class="pageForm required-validate" onsubmit="return iframeCallback(this);">
	<fieldset>
		<legend>基本信息</legend>
		<dl>
			<dt>键:</dt>
			<dd><input readonly="readonly" value="${domain.kee }" class="required" name="kee" type="text" /></dd>
		</dl>
		<dl class="nowrap">
			<dt>值:</dt>
				<dd>
				<c:choose>
					<c:when test="${domain.type ==  1}">
						<input value="${domain.value }" class="required" name="value" type="text" />
					</c:when>
					<c:when test="${domain.type ==  5 || domain.type ==  4}">
						<input value="${domain.value }" class="required" name="value" type="number" />
					</c:when>
					<c:when test="${domain.type ==  2 }">
						<input readonly="readonly" value="<fmt:formatDate value="${domain.value }" pattern="yyyy-MM-dd"/>" class="required date" name="value" type="text" />
					</c:when>
					<c:when test="${domain.type ==  3 }">
						<input readonly="readonly" value="<fmt:formatDate value="${domain.value }" pattern="yyyy-MM-dd HH:mm:ss"/>" class="required date" name="value" type="text" />
					</c:when>
					<c:when test="${domain.type ==  6 }">
					<textarea name="value" rows="8" cols="80">${domain.value }</textarea>
					</c:when>
				</c:choose>
			</dd>
		</dl>
	</fieldset>
	<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
</form>
</div>
</div>