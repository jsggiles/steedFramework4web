<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/head/tagLib.jsp" %>
<div class="pageHeader">
	<form id="ConfigForm" rel="pagerForm" onsubmit="return navTabSearch(this);" action="<st:getCurrentUrl lastUrl="index.act" />" method="post">
	<div class="searchBar">
		<table class="searchContent">
			<tr>
			<td></td>
				<td>
					参数名:&nbsp;&nbsp;&nbsp;<input name="kee" type="text" />
				</td>
				<td>
					参数值:&nbsp;&nbsp;&nbsp;<input name="value" type="text" />
				</td>
				
			</tr>
		</table>
		<div class="subBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">查询</button></div></div></li>
			</ul>
		</div>
	</div>
	
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="edit" rel="editConfig" href="<st:getCurrentUrl lastUrl="edit.act" />?kee={sid_Config}" title="修改参数" target="navTab"><span>修改</span></a></li>
		</ul>
	</div>
	<div id="w_list_print_Config">
	<table class="table" width="100%" layoutH="138">
		<thead>
			<tr>
				<th align="center">键</th>
				<th style="max-width: 400px;" align="center">值</th>
			</tr>
		</thead>
		<tbody>
		      <c:forEach items="${page.domainCollection}" var="temp">
			      <tr target="sid_Config" rel='<st:encodUrl url="${temp.kee }" />'>
				      <td> 
					      ${temp.kee}
					  </td>
				  
				      <td style="max-width: 400px;"> 
				          ${temp.value }
				       </td>
			    	</tr>
		    </c:forEach>
		</tbody>
	</table>
	</div>
	<%@include file="/WEB-INF/jsp/include/plugin/page.jsp"%>
	<st:FillInputByParam root="ConfigForm" />
</div>

