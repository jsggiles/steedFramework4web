package steed.util.redis;

import java.io.File;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import steed.exception.runtime.system.SystemInitException;
import steed.util.base.BaseUtil;
import steed.util.base.PathUtil;

public class RedissonUtil {
	public static RedissonClient redisson;
	static{
		Config config;
		try {
			config = Config.fromJSON(new File(PathUtil.praseRelativePath("properties/redisson.json")));
			redisson = Redisson.create(config);
			if (redisson.isShutdown()) {
				BaseUtil.getLogger().warn("Redis服务未启动!!");
			}
//			redisson.getMap("aa").c
		} catch (Exception e) {
			throw new SystemInitException("Redisson配置错误!", e);
		}
	}
	
	public static RedissonClient getRedisson() {
		return redisson;
	}
}
