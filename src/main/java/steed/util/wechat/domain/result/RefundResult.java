package steed.util.wechat.domain.result;

public class RefundResult extends BaseWechatResult{
	private String return_code;// 	是 	String(16) 	SUCCESS 	SUCCESS/FAIL
	private String return_msg;// 	否 	String(128) 	签名失败 	返回信息，如非空，为错误原因,签名失败,参数格式校验错误,以下字段在return_code为SUCCESS的时候有返回
	private String result_code;// 	是 	String(16) 	SUCCESS  SUCCESS/FAIL SUCCESS退款申请接收成功，结果通过退款查询接口查询 FAIL 提交业务失败
	private String err_code;//	否 	String(32) 	SYSTEMERROR 	列表详见错误码列表
	private String err_code_des;// 	否 	String(128) 	系统超时 	结果信息描述
	private String appid;// 	是 	String(32) 	wx8888888888888888 	微信分配的小程序ID
	private String mch_id;// 	是 	String(32) 	1900000109 	微信支付分配的商户号
	private String device_info;// 	否 	String(32) 	013467007045764 	微信支付分配的终端设备号，与下单一致
	private String nonce_str;// 	是 	String(32) 	5K8264ILTKCH16CQ2502SI8ZNMTM67VS 	随机字符串，不长于32位
	private String sign;// 	是 	String(32) 	5K8264ILTKCH16CQ2502SI8ZNMTM67VS 	签名，详见签名算法
	private String transaction_id;// 	是 	String(28) 	4007752501201407033233368018 	微信订单号
	private String out_trade_no;// 	是 	String(32) 	33368018 	商户系统内部的订单号
	private String out_refund_no;// 	是 	String(32) 	121775250 	商户退款单号
	private String refund_id;// 	是 	String(28) 	2007752501201407033233368018 	微信退款单号
	private String refund_channel;// 	否 	String(16) 	ORIGINAL  ORIGINAL—原路退款 BALANCE—退回到余额
	private Integer refund_fee;// 	是 	Int 	100 	退款总金额,单位为分,可以做部分退款
	private String settlement_refund_fee;// 	否 	Int 	100 	去掉非充值代金券退款金额后的退款金额，退款金额=申请退款金额-非充值代金券退款金额，退款金额<=申请退款金额
	private Integer total_fee;// 	是 	Int 	100 	订单总金额，单位为分，只能为整数，详见支付金额
	private String settlement_total_fee;// 	否 	Int 	100 	去掉非充值代金券金额后的订单总金额，应结订单金额=订单金额-非充值代金券金额，应结订单金额<=订单金额。
	private String fee_type;// 	否 	String(8) 	CNY 	订单金额货币类型，符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
	private Integer cash_fee;// 	是 	Int 	100 	现金支付金额，单位为分，只能为整数，详见支付金额
	private Integer cash_refund_fee;// 	否 	Int 	100 	现金退款金额，单位为分，只能为整数，详见支付金额
	
	private String coupon_type_0;// 	否 	String(8) 	CASH  CASH--充值代金券  NO_CASH---非充值代金券 订单使用代金券时有返回（取值：CASH、NO_CASH）。$n为下标,从0开始编号，举例：coupon_type_0
	private String coupon_type_1;
	private String coupon_type_2;
	private String coupon_type_3;
	private String coupon_type_4;
	private String coupon_type_5;
	private String coupon_type_6;
	
	
	private Integer coupon_refund_fee;// 	否 	Int 	100 	代金券退款金额<=退款金额，退款金额-代金券或立减优惠退款金额为现金，说明详见代金券或立减优惠
	private Integer coupon_refund_fee_0;// 	否 	Int 	100 	代金券退款金额<=退款金额，退款金额-代金券或立减优惠退款金额为现金，说明详见代金券或立减优惠
	private Integer coupon_refund_fee_1;
	private Integer coupon_refund_fee_2;
	private Integer coupon_refund_fee_3;
	private Integer coupon_refund_fee_4;
	private Integer coupon_refund_fee_5;
	private Integer coupon_refund_fee_6;
	
	
	private Integer coupon_refund_count;
	private Integer coupon_refund_count_0;// 	否 	Int 	1 	退款代金券使用数量 ,$n为下标,从0开始编号
	private Integer coupon_refund_count_1;
	private Integer coupon_refund_count_2;
	private Integer coupon_refund_count_3;
	private Integer coupon_refund_count_4;
	private Integer coupon_refund_count_5;
	private Integer coupon_refund_count_6;
	
	private String coupon_refund_batch_id;
	private String coupon_refund_batch_id_0_0;// 	否 	String(20) 	100 	退款代金券批次ID ,$n为下标，$m为下标，从0开始编号
	private String coupon_refund_batch_id_1_1;
	private String coupon_refund_batch_id_2_2;
	private String coupon_refund_batch_id_3_3;
	private String coupon_refund_batch_id_4_4;
	private String coupon_refund_batch_id_5_5;
	private String coupon_refund_batch_id_6_6;
	
	private String coupon_refund_id;
	private String coupon_refund_id_0_0;// 	否 	String(20) 	10000  	退款代金券ID, $n为下标，$m为下标，从0开始编号
	private String coupon_refund_id_1_1;
	private String coupon_refund_id_2_2;
	private String coupon_refund_id_3_3;
	private String coupon_refund_id_4_4;
	private String coupon_refund_id_5_5;
	private String coupon_refund_id_6_6;
	
	
	private Integer coupon_refund_fee_0_0;// 	否 	Int 	100 	单个退款代金券支付金额, $n为下标，$m为下标，从0开始编号
	private Integer coupon_refund_fee_1_1;
	private Integer coupon_refund_fee_2_2;
	private Integer coupon_refund_fee_3_3;
	private Integer coupon_refund_fee_4_4;
	private Integer coupon_refund_fee_5_5;
	private Integer coupon_refund_fee_6_6;
	public String getReturn_code() {
		return return_code;
	}
	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}
	public String getReturn_msg() {
		return return_msg;
	}
	public void setReturn_msg(String return_msg) {
		this.return_msg = return_msg;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getErr_code() {
		return err_code;
	}
	public void setErr_code(String err_code) {
		this.err_code = err_code;
	}
	public String getErr_code_des() {
		return err_code_des;
	}
	public void setErr_code_des(String err_code_des) {
		this.err_code_des = err_code_des;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getMch_id() {
		return mch_id;
	}
	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}
	public String getDevice_info() {
		return device_info;
	}
	public void setDevice_info(String device_info) {
		this.device_info = device_info;
	}
	public String getNonce_str() {
		return nonce_str;
	}
	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}
	public String getSign() {
		return sign;
	}
	public Integer getCoupon_refund_fee() {
		return coupon_refund_fee;
	}
	public void setCoupon_refund_fee(Integer coupon_refund_fee) {
		this.coupon_refund_fee = coupon_refund_fee;
	}
	public Integer getCoupon_refund_count() {
		return coupon_refund_count;
	}
	public void setCoupon_refund_count(Integer coupon_refund_count) {
		this.coupon_refund_count = coupon_refund_count;
	}
	public String getCoupon_refund_batch_id() {
		return coupon_refund_batch_id;
	}
	public void setCoupon_refund_batch_id(String coupon_refund_batch_id) {
		this.coupon_refund_batch_id = coupon_refund_batch_id;
	}
	public String getCoupon_refund_id() {
		return coupon_refund_id;
	}
	public void setCoupon_refund_id(String coupon_refund_id) {
		this.coupon_refund_id = coupon_refund_id;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getOut_trade_no() {
		return out_trade_no;
	}
	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}
	public String getOut_refund_no() {
		return out_refund_no;
	}
	public void setOut_refund_no(String out_refund_no) {
		this.out_refund_no = out_refund_no;
	}
	public String getRefund_id() {
		return refund_id;
	}
	public void setRefund_id(String refund_id) {
		this.refund_id = refund_id;
	}
	public String getRefund_channel() {
		return refund_channel;
	}
	public void setRefund_channel(String refund_channel) {
		this.refund_channel = refund_channel;
	}
	public Integer getRefund_fee() {
		return refund_fee;
	}
	public void setRefund_fee(Integer refund_fee) {
		this.refund_fee = refund_fee;
	}
	public String getSettlement_refund_fee() {
		return settlement_refund_fee;
	}
	public void setSettlement_refund_fee(String settlement_refund_fee) {
		this.settlement_refund_fee = settlement_refund_fee;
	}
	public Integer getTotal_fee() {
		return total_fee;
	}
	public void setTotal_fee(Integer total_fee) {
		this.total_fee = total_fee;
	}
	public String getSettlement_total_fee() {
		return settlement_total_fee;
	}
	public void setSettlement_total_fee(String settlement_total_fee) {
		this.settlement_total_fee = settlement_total_fee;
	}
	public String getFee_type() {
		return fee_type;
	}
	public void setFee_type(String fee_type) {
		this.fee_type = fee_type;
	}
	public Integer getCash_fee() {
		return cash_fee;
	}
	public void setCash_fee(Integer cash_fee) {
		this.cash_fee = cash_fee;
	}
	public Integer getCash_refund_fee() {
		return cash_refund_fee;
	}
	public void setCash_refund_fee(Integer cash_refund_fee) {
		this.cash_refund_fee = cash_refund_fee;
	}
	public String getCoupon_type_0() {
		return coupon_type_0;
	}
	public void setCoupon_type_0(String coupon_type_0) {
		this.coupon_type_0 = coupon_type_0;
	}
	public String getCoupon_type_1() {
		return coupon_type_1;
	}
	public void setCoupon_type_1(String coupon_type_1) {
		this.coupon_type_1 = coupon_type_1;
	}
	public String getCoupon_type_2() {
		return coupon_type_2;
	}
	public void setCoupon_type_2(String coupon_type_2) {
		this.coupon_type_2 = coupon_type_2;
	}
	public String getCoupon_type_3() {
		return coupon_type_3;
	}
	public void setCoupon_type_3(String coupon_type_3) {
		this.coupon_type_3 = coupon_type_3;
	}
	public String getCoupon_type_4() {
		return coupon_type_4;
	}
	public void setCoupon_type_4(String coupon_type_4) {
		this.coupon_type_4 = coupon_type_4;
	}
	public String getCoupon_type_5() {
		return coupon_type_5;
	}
	public void setCoupon_type_5(String coupon_type_5) {
		this.coupon_type_5 = coupon_type_5;
	}
	public String getCoupon_type_6() {
		return coupon_type_6;
	}
	public void setCoupon_type_6(String coupon_type_6) {
		this.coupon_type_6 = coupon_type_6;
	}
	public Integer getCoupon_refund_fee_0() {
		return coupon_refund_fee_0;
	}
	public void setCoupon_refund_fee_0(Integer coupon_refund_fee_0) {
		this.coupon_refund_fee_0 = coupon_refund_fee_0;
	}
	public Integer getCoupon_refund_fee_1() {
		return coupon_refund_fee_1;
	}
	public void setCoupon_refund_fee_1(Integer coupon_refund_fee_1) {
		this.coupon_refund_fee_1 = coupon_refund_fee_1;
	}
	public Integer getCoupon_refund_fee_2() {
		return coupon_refund_fee_2;
	}
	public void setCoupon_refund_fee_2(Integer coupon_refund_fee_2) {
		this.coupon_refund_fee_2 = coupon_refund_fee_2;
	}
	public Integer getCoupon_refund_fee_3() {
		return coupon_refund_fee_3;
	}
	public void setCoupon_refund_fee_3(Integer coupon_refund_fee_3) {
		this.coupon_refund_fee_3 = coupon_refund_fee_3;
	}
	public Integer getCoupon_refund_fee_4() {
		return coupon_refund_fee_4;
	}
	public void setCoupon_refund_fee_4(Integer coupon_refund_fee_4) {
		this.coupon_refund_fee_4 = coupon_refund_fee_4;
	}
	public Integer getCoupon_refund_fee_5() {
		return coupon_refund_fee_5;
	}
	public void setCoupon_refund_fee_5(Integer coupon_refund_fee_5) {
		this.coupon_refund_fee_5 = coupon_refund_fee_5;
	}
	public Integer getCoupon_refund_fee_6() {
		return coupon_refund_fee_6;
	}
	public void setCoupon_refund_fee_6(Integer coupon_refund_fee_6) {
		this.coupon_refund_fee_6 = coupon_refund_fee_6;
	}
	public Integer getCoupon_refund_count_0() {
		return coupon_refund_count_0;
	}
	public void setCoupon_refund_count_0(Integer coupon_refund_count_0) {
		this.coupon_refund_count_0 = coupon_refund_count_0;
	}
	public Integer getCoupon_refund_count_1() {
		return coupon_refund_count_1;
	}
	public void setCoupon_refund_count_1(Integer coupon_refund_count_1) {
		this.coupon_refund_count_1 = coupon_refund_count_1;
	}
	public Integer getCoupon_refund_count_2() {
		return coupon_refund_count_2;
	}
	public void setCoupon_refund_count_2(Integer coupon_refund_count_2) {
		this.coupon_refund_count_2 = coupon_refund_count_2;
	}
	public Integer getCoupon_refund_count_3() {
		return coupon_refund_count_3;
	}
	public void setCoupon_refund_count_3(Integer coupon_refund_count_3) {
		this.coupon_refund_count_3 = coupon_refund_count_3;
	}
	public Integer getCoupon_refund_count_4() {
		return coupon_refund_count_4;
	}
	public void setCoupon_refund_count_4(Integer coupon_refund_count_4) {
		this.coupon_refund_count_4 = coupon_refund_count_4;
	}
	public Integer getCoupon_refund_count_5() {
		return coupon_refund_count_5;
	}
	public void setCoupon_refund_count_5(Integer coupon_refund_count_5) {
		this.coupon_refund_count_5 = coupon_refund_count_5;
	}
	public Integer getCoupon_refund_count_6() {
		return coupon_refund_count_6;
	}
	public void setCoupon_refund_count_6(Integer coupon_refund_count_6) {
		this.coupon_refund_count_6 = coupon_refund_count_6;
	}
	public String getCoupon_refund_batch_id_0_0() {
		return coupon_refund_batch_id_0_0;
	}
	public void setCoupon_refund_batch_id_0_0(String coupon_refund_batch_id_0_0) {
		this.coupon_refund_batch_id_0_0 = coupon_refund_batch_id_0_0;
	}
	public String getCoupon_refund_batch_id_1_1() {
		return coupon_refund_batch_id_1_1;
	}
	public void setCoupon_refund_batch_id_1_1(String coupon_refund_batch_id_1_1) {
		this.coupon_refund_batch_id_1_1 = coupon_refund_batch_id_1_1;
	}
	public String getCoupon_refund_batch_id_2_2() {
		return coupon_refund_batch_id_2_2;
	}
	public void setCoupon_refund_batch_id_2_2(String coupon_refund_batch_id_2_2) {
		this.coupon_refund_batch_id_2_2 = coupon_refund_batch_id_2_2;
	}
	public String getCoupon_refund_batch_id_3_3() {
		return coupon_refund_batch_id_3_3;
	}
	public void setCoupon_refund_batch_id_3_3(String coupon_refund_batch_id_3_3) {
		this.coupon_refund_batch_id_3_3 = coupon_refund_batch_id_3_3;
	}
	public String getCoupon_refund_batch_id_4_4() {
		return coupon_refund_batch_id_4_4;
	}
	public void setCoupon_refund_batch_id_4_4(String coupon_refund_batch_id_4_4) {
		this.coupon_refund_batch_id_4_4 = coupon_refund_batch_id_4_4;
	}
	public String getCoupon_refund_batch_id_5_5() {
		return coupon_refund_batch_id_5_5;
	}
	public void setCoupon_refund_batch_id_5_5(String coupon_refund_batch_id_5_5) {
		this.coupon_refund_batch_id_5_5 = coupon_refund_batch_id_5_5;
	}
	public String getCoupon_refund_batch_id_6_6() {
		return coupon_refund_batch_id_6_6;
	}
	public void setCoupon_refund_batch_id_6_6(String coupon_refund_batch_id_6_6) {
		this.coupon_refund_batch_id_6_6 = coupon_refund_batch_id_6_6;
	}
	public String getCoupon_refund_id_0_0() {
		return coupon_refund_id_0_0;
	}
	public void setCoupon_refund_id_0_0(String coupon_refund_id_0_0) {
		this.coupon_refund_id_0_0 = coupon_refund_id_0_0;
	}
	public String getCoupon_refund_id_1_1() {
		return coupon_refund_id_1_1;
	}
	public void setCoupon_refund_id_1_1(String coupon_refund_id_1_1) {
		this.coupon_refund_id_1_1 = coupon_refund_id_1_1;
	}
	public String getCoupon_refund_id_2_2() {
		return coupon_refund_id_2_2;
	}
	public void setCoupon_refund_id_2_2(String coupon_refund_id_2_2) {
		this.coupon_refund_id_2_2 = coupon_refund_id_2_2;
	}
	public String getCoupon_refund_id_3_3() {
		return coupon_refund_id_3_3;
	}
	public void setCoupon_refund_id_3_3(String coupon_refund_id_3_3) {
		this.coupon_refund_id_3_3 = coupon_refund_id_3_3;
	}
	public String getCoupon_refund_id_4_4() {
		return coupon_refund_id_4_4;
	}
	public void setCoupon_refund_id_4_4(String coupon_refund_id_4_4) {
		this.coupon_refund_id_4_4 = coupon_refund_id_4_4;
	}
	public String getCoupon_refund_id_5_5() {
		return coupon_refund_id_5_5;
	}
	public void setCoupon_refund_id_5_5(String coupon_refund_id_5_5) {
		this.coupon_refund_id_5_5 = coupon_refund_id_5_5;
	}
	public String getCoupon_refund_id_6_6() {
		return coupon_refund_id_6_6;
	}
	public void setCoupon_refund_id_6_6(String coupon_refund_id_6_6) {
		this.coupon_refund_id_6_6 = coupon_refund_id_6_6;
	}
	public Integer getCoupon_refund_fee_0_0() {
		return coupon_refund_fee_0_0;
	}
	public void setCoupon_refund_fee_0_0(Integer coupon_refund_fee_0_0) {
		this.coupon_refund_fee_0_0 = coupon_refund_fee_0_0;
	}
	public Integer getCoupon_refund_fee_1_1() {
		return coupon_refund_fee_1_1;
	}
	public void setCoupon_refund_fee_1_1(Integer coupon_refund_fee_1_1) {
		this.coupon_refund_fee_1_1 = coupon_refund_fee_1_1;
	}
	public Integer getCoupon_refund_fee_2_2() {
		return coupon_refund_fee_2_2;
	}
	public void setCoupon_refund_fee_2_2(Integer coupon_refund_fee_2_2) {
		this.coupon_refund_fee_2_2 = coupon_refund_fee_2_2;
	}
	public Integer getCoupon_refund_fee_3_3() {
		return coupon_refund_fee_3_3;
	}
	public void setCoupon_refund_fee_3_3(Integer coupon_refund_fee_3_3) {
		this.coupon_refund_fee_3_3 = coupon_refund_fee_3_3;
	}
	public Integer getCoupon_refund_fee_4_4() {
		return coupon_refund_fee_4_4;
	}
	public void setCoupon_refund_fee_4_4(Integer coupon_refund_fee_4_4) {
		this.coupon_refund_fee_4_4 = coupon_refund_fee_4_4;
	}
	public Integer getCoupon_refund_fee_5_5() {
		return coupon_refund_fee_5_5;
	}
	public void setCoupon_refund_fee_5_5(Integer coupon_refund_fee_5_5) {
		this.coupon_refund_fee_5_5 = coupon_refund_fee_5_5;
	}
	public Integer getCoupon_refund_fee_6_6() {
		return coupon_refund_fee_6_6;
	}
	public void setCoupon_refund_fee_6_6(Integer coupon_refund_fee_6_6) {
		this.coupon_refund_fee_6_6 = coupon_refund_fee_6_6;
	}
	
}
