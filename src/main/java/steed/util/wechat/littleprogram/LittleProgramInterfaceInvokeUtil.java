package steed.util.wechat.littleprogram;

import steed.util.wechat.WechatInterfaceInvokeUtil;
import steed.util.wechat.domain.result.BaseWechatResult;
import steed.util.wechat.littleprogram.domain.result.LoginResult;
import steed.util.wechat.littleprogram.domain.send.TemplateMessage;

/**
 * 小程序接口调用工具
 * @author 战马
 */
public class LittleProgramInterfaceInvokeUtil {
	
	public static LoginResult login(String code){
		return WechatInterfaceInvokeUtil.invokeWechatInterface(null, LoginResult.class, "https://api.weixin.qq.com/sns/jscode2session?appid=#APPID#&secret=#APPSECRET#&js_code=#JSCODE#&grant_type=authorization_code".replace("#JSCODE#", code));
	}
	
	public static BaseWechatResult sendTemplateMessage(TemplateMessage templateMessage){
		return WechatInterfaceInvokeUtil.invokeWechatInterface(templateMessage, BaseWechatResult.class, "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=#ACCESS_TOKEN#");
	}
	
}
