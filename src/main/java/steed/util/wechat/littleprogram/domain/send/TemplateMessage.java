package steed.util.wechat.littleprogram.domain.send;

import java.util.HashMap;
import java.util.Map;

import steed.util.wechat.domain.send.BaseWechatSend;

/**
 * touser 	是 	接收者（用户）的 openid
	template_id 	是 	所需下发的模板消息的id
	page 	否 	点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
	form_id 	是 	表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id
	data 	是 	模板内容，不填则下发空模板
	color 	否 	模板内容字体的颜色，不填默认黑色
	emphasis_keyword 	否 	模板需要放大的关键词，不填则默认无放大
 * @author 战马
 *
 */
public class TemplateMessage extends BaseWechatSend{
	private String touser;
	private String template_id;
	private String page;
	private String form_id;
	private Map<String, KeyWord> data = new HashMap<>();
	private String emphasis_keyword;
	
	private transient int count;
	
	public String getTouser() {
		return touser;
	}

	public void setTouser(String touser) {
		this.touser = touser;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public String getPage() {
		return page;
	}
	/**
	 * 按顺序添加data段数据,第一次调用该方法会添加keyword1的数据,第二次调用则添加keyword2的数据............
	 * 
	 * @param value 值
	 * @param color 颜色,传null则用默认的黑色
	 */
	public void addData(String value,String color){
		data.put("keyword"+ ++count, new KeyWord(value, color));
	}
	/**
	 * 按顺序添加data段数据,第一次调用该方法会添加keyword1的数据,第二次调用则添加keyword2的数据............
	 * @param value 值
	 */
	public void addData(String value){
		addData(value, null);
	}
	
	public void setPage(String page) {
		this.page = page;
	}

	public String getForm_id() {
		return form_id;
	}

	public void setForm_id(String form_id) {
		this.form_id = form_id;
	}

	public String getEmphasis_keyword() {
		return emphasis_keyword;
	}

	public void setEmphasis_keyword(String emphasis_keyword) {
		this.emphasis_keyword = emphasis_keyword;
	}


	public class KeyWord{
		String value;
		String color;
		public KeyWord(String value, String color) {
			this.value = value;
			this.color = color;
		}
		
	}
}
