package steed.util.file;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import steed.domain.BaseUser;
import steed.domain.GlobalParam;
import steed.exception.runtime.system.AttackedExeception;
import steed.util.base.PathUtil;
import steed.util.digest.DigestUtil;

public class AttachmentUtil {
	/**
	 * 保存附件
	 * @param file
	 * @param fileName 为null时fileName = file.getName();
	 * @param key 尽量不要用中文,为null时保存到公共目录，否则保存到key专用目录(一个key对应一个目录,key可以传用户的id,这样不同用户的附件会保存到不同目录)
	 * @return 相对于项目部署目录的相对路径 可以用PathUtil.getBrowserPath(steed.util.file.AttachmentUtil.saveGroupByKey())获取浏览器能够访问的路径
	 * 
	 * @see steed.util.base.PathUtil#getBrowserPath(String)
	 * @see #delete(String)
	 */
	public static String saveGroupByKey(File file,String fileName,String key){
		if (fileName == null) {
			fileName = file.getName();
		}
		String path = getPath(key,file,fileName);
		save(file, path);
		return path;
	}
	
	/**
	 * 保存附件
	 * @param file
	 * @param relativePath 相对于项目部署目录的相对路径 可以用PathUtil.getBrowserPath(relativePath)获取浏览器能够访问的路径
	 * 
	 * @see steed.util.base.PathUtil#getBrowserPath(String)
	 * @return 相对于项目部署目录的相对路径
	 */
	public static void save(File file,String relativePath){
		
		if (relativePath.endsWith(".jsp")) {
			throw new AttackedExeception("请勿上传jsp文件!!!");
		}
		FileUtil.copyFile(file, PathUtil.mergePath(GlobalParam.FOLDER.rootPath, relativePath));
	}
	
	/**
	 * 删除附件
	 * @param path 相对于项目部署目录的相对路径save的返回值
	 * @see #save(File, String, BaseUser)
	 */
	public static void delete(String path){
		new File(GlobalParam.FOLDER.rootPath+path).delete();
	}
	
	
	private static String getPath(String key,File file,String fileName) {
		String userPath;
		Date now = new Date();
		if (key != null) {
			userPath = key+"/";
		}else {
			SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd/");
			userPath = format.format(now);
		}
		String decodeUrl = DigestUtil.AESAndMd532MinEncode(String.format("%d%d", now.getTime(),new Random().nextInt()));
		return "/upload/"+userPath+"file/"+decodeUrl+FileUtil.getFileSuffix(fileName);
	}
	
}
