package steed.util.system;

import java.io.Serializable;

import steed.util.reflect.ReflectUtil;

/**
 * 缓存工具类
 * @author 战马
 *
 */
public class DataCacheUtil {
	private static DataCacheEngine cacheEngine = ReflectUtil.getInstanceFromProperties("dataCacheEngine");
	
	static{
		cacheEngine = new StandAloneMachineDataCacheEngine();
	}
	
	/**
	 * 获取缓存数据
	 * @param key
	 * @param prefix
	 * @return
	 */
	public static <T> T getData(String key,String prefix){
		return cacheEngine.getData(key, prefix);
	}
	
	/**
	 * 存放临时缓存数据,两小时过期
	 * @param key
	 * @param prefix
	 * @param value
	 */
	public static void setData(String key,String prefix,Serializable value){
		cacheEngine.setData(key, prefix, value);
	}
	/**
	 * 存放临时缓存数据
	 * @param key
	 * @param prefix
	 * @param value
	 * @param lifeTime 存活时间(毫秒)
	 */
	public static void setData(String key,String prefix,Serializable value,Long lifeTime){
		cacheEngine.setData(key, prefix, value, lifeTime);
	}
	/**
	 * 存放永久有效的缓存数据
	 * @param key
	 * @param prefix
	 * @param value
	 */
	public static void setPermanentData(String key,String prefix,Serializable value){
		cacheEngine.setPermanentData(key, prefix, value);
	}
	
	public interface DataCacheEngine{
		/**
		 * 存放临时缓存数据
		 * @param key
		 * @param prefix
		 * @param value
		 * @param lifeTime 存活时间(毫秒)
		 */
		public void setData(String key,String prefix,Serializable value,Long lifeTime);
		/**
		 * 存放临时缓存数据,两小时过期
		 * @param key
		 * @param prefix
		 * @param value
		 */
		public void setData(String key,String prefix,Serializable value);
		/**
		 * 存放永久有效的缓存数据
		 * @param key
		 * @param prefix
		 * @param value
		 */
		public void setPermanentData(String key,String prefix,Serializable value);
		/**
		 * 获取缓存数据
		 * @param key
		 * @param prefix
		 * @return
		 */
		public <T> T getData(String key,String prefix);
	}
	
}
