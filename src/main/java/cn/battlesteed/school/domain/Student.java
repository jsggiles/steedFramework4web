package cn.battlesteed.school.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import steed.domain.BaseRelationalDatabaseDomain;

/**
 * 学生
 * @author 战马
 *
 */
@Entity
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Student extends BaseRelationalDatabaseDomain{
	private static final long serialVersionUID = -2258584179191606674L;
	private Long id;
	private String name;
	private String learnNumber;
	private String description;
	
	@Id
	@GenericGenerator(name="gen1",strategy="native")
	@GeneratedValue(generator="gen1")
	public Long getId() {
		return id;
	}

	public String getLearnNumber() {
		return learnNumber;
	}

	public void setLearnNumber(String learnNumber) {
		this.learnNumber = learnNumber;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	

}
